/** @file 
 * @brief Plik zawierający funkcję main.
 * @author Justyna Micota <jm418427@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 10.05.2020
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "base.h"
#include "operations.h"
#include "gamma.h"
#include "input.h"
#include "batch.h"
#include "interactive.h"

/** @brief Pozwala na przeprowadzenie gry.
 * Wywołuje funkcję get_first_line definiującą tryb oraz parametry gry
 * tak długo, aż wejście będzie poprawne. Następnie wywołuje funkcje
 * przeprowadzające grę w odpowiednim trybie (batch lub interactive).
 * Zwraca informacje o błędach wraz z numerem linii, w którym wystąpiły.
 * Nie pozwala na przeprowadzenie gry na błędnych danych.
 * @return Zero, gdy wszystko przebiegło poprawnie,
 * a w przeciwnym przypadku kod zakończenia programu jest kodem błędu.
 */
int main() {
    uint32_t height = 0, width = 0, players = 0, areas = 0;
    uint32_t line_counter = 0;
    char mode = 'x';
    int a = 0; // zmienna przechowująca ostatni znak pobrany z wejścia
    bool success = false;
    gamma_t* gamma = NULL;

    // dopóki wczytanie pierwszej linii nie zakończy się sukcesem, bądź
    // do czasu gdy skończą się dane na wejściu
    while (a != EOF && (!success)) {

        success = get_first_line(&line_counter, &a, &mode, &width, &height, 
                                 &players, &areas);

        if (success && a != EOF) {
            if (mode == '#' || mode == '\n') {
                success = false;
            }
            else {
                // próba utworzenia rozgrywki
                gamma = gamma_new(width, height, players, areas);
                if (gamma == NULL) {
                    success = false;
                }
            }
        }

        if (mode != '#' && mode != '\n' && !success) {
            fprintf(stderr, "ERROR %d\n", line_counter);
        }
    }

    // rozpoczęcie gry w odpowiednim trybie
    if (mode == 'B') {
        start_batch(&a, &line_counter, gamma);
    }
    else if (mode == 'I') {
        start_interactive(gamma);
    }

    gamma_delete(gamma);
    return 0;
}