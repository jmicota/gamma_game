/** @file
 * @brief Interfejs klasy przechowującej podstawowe elementy projektu.
 * Zawiera wszystkie struktury, metody tworzące nowe puste struktury pól 
 * i graczy, sprawdzenia poprawności parametrów, proste operacje na tablicach 
 * oraz proste operacje arytmetyczne.
 * @author Justyna Micota <jm418427@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 14.04.2020
 */

#ifndef BASE_H
#define BASE_H

/** @brief Struktura reprezentująca pole na planszy.
 */
struct field {
    uint32_t player_owning; ///< gracz którego pionek zajmuje to pole
    struct field* parent_field; ///< pole "wyżej" w drzewie find and union
    uint32_t rank; ///< ranga wg optymalizacji algorytmu find and union
    bool visited; ///< wartość orientacyjna dla algorytmu dfs
    int fill_distance; ///< dodatkowe kroki aby przejść na sąsiednie pole
};
typedef struct field field_t; ///< struktura reprezentująca pole na planszy

/** @brief Struktura reprezentująca jednego gracza.
 */
struct player {
    bool used_golden_move; ///< wartość true, jeśli gracz wykonał złoty ruch
    uint32_t occupied_areas; ///< liczba zajmowanych przez gracza przestrzeni
    uint32_t occupied_fields; ///< liczba zajmowanych przez gracza pól
    uint32_t adjacent_empty_fields; ///< puste pola sąsiadujące z polami gracza
};
typedef struct player player_t; ///< struktura reprezentująca jednego gracza

/** @brief Struktura reprezentująca grę gamma.
 */
struct gamma {
    uint32_t nr_of_players; ///< liczba wszystkich graczy (liczba dodatnia)
    uint32_t width; ///< szerokość planszy (liczba dodatnia)
    uint32_t height; ///< wysokość planszy (liczba dodatnia)
    uint32_t areas; ///< dopuszczana liczba zajmowanych przestrzeni (dodatnia)
    uint32_t all_free_fields; ///< liczba aktualnych pustych pól na planszy

    field_t** fields; ///< tablica struktur wszystkich pól
    player_t** players; ///< tablica struktur wszystkich graczy
};
typedef struct gamma gamma_t; ///< struktura reprezentująca grę gamma

/** @brief Alokuje nowe pole.
 * @return Wskaźnik na zaalokowaną strukturę pola.
 */
field_t* field_new();

/** @brief Alokuje nowego gracza.
 * @return Wskaźnik na zaalokowaną strukturę gracza.
 */
player_t* player_new();

/** @brief Oblicza liczbę cyfr w dziesiętnej reprezentacji x.
 * @return Liczba cyfr w dziesiętnej reprezentacji x.
 */
int numbers_length(uint32_t x);

/** @brief Oblicza indeks pola w tablicy gamma->fields z koordynatów x i y.
 * @param[in] g         – wskaźnik na strukturę przechowującą stan gry,
 * @param[in] x         – numer kolumny,
 * @param[in] y         – numer wiersza,
 * @return Indeks pola lub -1 w przypadku niepoprawnych argumentów.
 */
long long int calculate_field_index(gamma_t* g, long long int x, 
                                    long long int y);

/** @brief Sprawdza poprawność wartości indeksu pola.
 * @param[in] gamma         – wskaźnik na strukturę przechowującą stan gry,
 * @param[in] field_index   – indeks pola.
 * @return Wartość @p true gdy wartość field_index jest nieujemna i mniejsza
 * od liczby wszystkich pól w grze, a @p false w przeciwnym przypadku.
 */
bool correct_field_index(long long int field_index, gamma_t* gamma);

/** @brief Jeśli wartość numeru gracza zajmującego pole jest większa niż 1,
 * to ustawia wartość fill_distance danego pola na liczbę cyfr numeru gracza
 * + 1.
 * Jest to niezbędne do właściwego poruszania się po planszy dla graczy o 
 * numerze większym od 9.
 * @param[in] gamma         – wskaźnik na strukturę przechowującą stan gry,
 * @param[in] x             – numer kolumny pola,
 * @param[in] y             – numer wiersza pola.
 */
void set_fill_distance_of_field(uint32_t x, uint32_t y, gamma_t* gamma);

/** @brief Sprawdza, czy gracz może rozważać złoty ruch na pole.
 * Jeśli właścicielem pola jest sam @p player_number lub nikt, to gracz
 * @p player_number nie może wykonać złotego ruchu na to pole. 
 * @param[in] field         – wskaźnik na strukturę rozpatrywanego pola,
 * @param[in] player_number – numer gracza.
 * @return Wartość @p true, jeśli field->player_owning jest liczbą dodatnią, 
 * mniejszą lub równą liczbie graczy w grze, oraz inną niż @p player_number, 
 * a @p false w przeciwnym przypadku.
 */
bool correct_prev_owner(field_t* field, uint32_t player_number);

/** @brief Wypełnia tablicę koordynatów x i y dla sąsiadujących pól.
 * @param[in] x                 – numer kolumny w której jest badane pole,
 * @param[in] y                 – numer wiersza w którym jest badane pole,
 * @param[in,out] neighbour_x   – czteroelementowa tablica numerów kolumn pól 
 *                                sąsiadujących z badanym polem,
 * @param[in,out] neighbour_y   – czteroelementowa tablica numerów wierszy pól 
 *                                sąsiadujących z badanym polem.
 * Pola w tablicach neighbour_..[4] reprezentują sąsiadujące pola w następującej
 * kolejności: górne, prawe, dolne, lewe.
 */
void fill_neighbour_coordinates(long long int neighbour_x[4],
                                long long int neighbour_y[4], 
                                long long int x, long long int y);

/** @brief Oblicza ile razy x występuje w tablicy array[4].
 * @return Liczba wystąpień x w tablicy array[4].
 */
int how_many_times_appears_in_array(uint32_t x, uint32_t array[4]);

/** @brief Wypełnia tablice opisujące pola sąsiadujące z badanym polem.
 * @param[in] x                     – numer kolumny w której jest badane pole,
 * @param[in] y                     – numer wiersza w którym jest badane pole,
 * @param[in,out] neighbour_fields  – czteroelementowa tablica pól sąsiadujących
 *                                    z badanym polem,
 * @param[in,out] indexes           – czteroelementowa tablica indeksów pól 
 *                                    sąsiadujących z badanym polem,
 * @param[in,out] owners            – czteroelementowa tablica graczy na polach 
 *                                    sąsiadujących z badanym polem,
 * @param[in] g                     – wskaźnik na strukturę przechowującą grę.
 * Czteroelementowe tablice reprezentują sąsiadujące pola w następującej
 * kolejności: górne, prawe, dolne, lewe.
 */
void fill_neighbour_arrays(uint32_t x, uint32_t y, uint32_t owners[4], 
                            long long int indexes[4], 
                            field_t* neighbour_fields[4], gamma_t* g); 

/** @brief Sprawdza poprawność podstawowych parametrów ruchu.
 * @param[in] x                 – numer kolumny pola docelowego,
 * @param[in] y                 – numer wiersza pola docelowego,
 * @param[in] player            – numer gracza,
 * @param[in] g                 – wskaźnik na strukturę przechowującą stan gry.
 * @return Wartość @p true jeśli x oraz y nieujemne i mniejsze odpowiednio od
 * szerokości i wysokości planszy, oraz wartość player jest dodatnia i mniejsza
 * lub równa liczbie graczy w grze, a @p false w przeciwnym wypadku.
 */
bool correct_parametres(uint32_t x, uint32_t y, uint32_t player, gamma_t* g);

/** @brief Wylicza 10 ^ (num_length - 1).
 * Ta liczba wykorzystywana jest jako zmniejszający się dzielnik 
 * przy tworzeniu gamma_board (zapisu stanu gry).
 * Dzięki niemu pozyskujemy kolejne cyfry zapisywanej do tablicy charów liczby.
 * @param[in] num_length        – ile cyfr ma liczba, którą chcemy zapisać.
 * @return 10 ^ (num_length - 1).
 */
uint32_t divisor(int num_length);

/** @brief Umieszcza znak w tablicy sign na pozycji index.
 * Wartość index jest zwiększana o jeden, aby zapewnić ciągłość 
 * funkcji gamma_board.
 * @param[in] a                – znak do wstawienia,
 * @param[in] sign             – wskaźnik na tablicę charów,
 * @param[in] index            – pozycja, na którą ma być wstawiony znak.
 */
void insert_char(char a, char* sign, int* index);

#endif /*BASE_H*/