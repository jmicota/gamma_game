#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "base.h"
#include "operations.h"
#include "gamma.h"
#include "input.h"

/* Pobiera jedną linię wejścia oraz sprawdza jej poprawność.
 * Zwraca wartość @p true gdy polecenie poprawne, @p false w przeciwnym
 * wypadku.
 */
static bool get_command_line(uint32_t* line_counter, int* a, char* mode, 
                             uint32_t* arg1, uint32_t* arg2, uint32_t* arg3) {
    (*line_counter)++;
    reset_values(mode, arg1, arg2, arg3);
    bool error = false;
    *a = getchar();

    // jeśli linia składa się jedynie z EOF, to jest poprawna (pusta)
    // ale pobieranie wejścia powinno zostać przerwane
    if (*a == EOF) {
        return true;
    }

    assign_mode(a, mode, &error, 0);
    if (*mode == '#' || *mode == '\n' || error) {
        skip_line(a);
    }
    else if (*a != EOF) {
        *a = getchar();
        if (*mode != 'p') {
            assign_number(a, arg1, &error);
            if (*mode == 'm' || *mode == 'g') {
                assign_number(a, arg2, &error);
                assign_number(a, arg3, &error);
            }
        }
        check_last_character(a, &error);
        skip_line(a);

        // jeśli linie nie kończy się na '\n'
        if (*a == EOF) {
            error = true;
        }
    }
    
    return !error;
}

/* Wywołuje odpowiednie funkcje wypełniające polecenie pobrane
 * z wejścia oraz wypisuje jego wynik na wyjście.
 */
static void make_action(char mode, uint32_t arg1, uint32_t arg2, 
                uint32_t arg3, gamma_t* gamma, uint32_t nr_of_line) {
    switch(mode) {
        case 'p': ;
            char* p = gamma_board(gamma);
            if (p == NULL) {
                fprintf(stderr, "ERROR %d\n", nr_of_line);
            }
            else {
                printf(p);
                free(p);
            }
            break;
        case 'q':
            if(gamma_golden_possible(gamma, arg1)) {
                printf("1\n");
            }
            else printf("0\n");
            break;
        case 'f':
            printf("%ld\n", gamma_free_fields(gamma, arg1));
            break;
        case 'b':
            printf("%ld\n", gamma_busy_fields(gamma, arg1));
            break;
        case 'g':
            if(gamma_golden_move(gamma, arg1, arg2, arg3)) {
                printf("1\n");
            }
            else printf("0\n");
            break;
        case 'm':
            if(gamma_move(gamma, arg1, arg2, arg3)) {
                printf("1\n");
            }
            else printf("0\n");
            break;
    }
}

void start_batch(int* a, uint32_t* line_counter, gamma_t* gamma) {
    printf("OK %d\n", *line_counter);
    char mode = 'x';
    uint32_t arg1 = 0, arg2 = 0, arg3 = 0;

    while (*a != EOF) {
        if (!get_command_line(line_counter, a, &mode, &arg1, &arg2, &arg3)) {
            fprintf(stderr, "ERROR %d\n", *line_counter);
        }
        else if (mode != '#' && mode != '\n' && *a != EOF) {
            make_action(mode, arg1, arg2, arg3, gamma, *line_counter);
        }
    }
}