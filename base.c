#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "base.h"

// allocates new field
field_t* field_new()
{
    field_t* field = malloc(sizeof(field_t));
    if (field != NULL) {
        field->parent_field = field;
        field->player_owning = 0;
        field->rank = 0;
        field->visited = false;
        field->fill_distance = 0;
    }
    
    return field;
}

// allocates new player
player_t* player_new() 
{
    player_t* player = malloc(sizeof(player_t));
    if (player != NULL) {
        player->adjacent_empty_fields = 0;
        player->occupied_areas = 0;
        player->occupied_fields = 0;
        player->used_golden_move = false;
    }

    return player;
}

// calculates number of digits in decimal representation of x
int numbers_length(uint32_t x)
{
    if (x == 0)
        return 1;

    else {
        int count = 0;
        while (x > 0) {
            ++count;
            x = x/10;
        }
        return count;
    }
}

long long int calculate_field_index(gamma_t* g, long long int x, 
                                    long long int y) 
{
    if (g == NULL || y < 0 || y >= g->height || x < 0 || x >= g->width)
        return -1;
    
    return ((g->height) - y - 1) * (g->width) + x;
}

bool correct_field_index(long long int field_index, gamma_t* gamma) 
{
    return (field_index != -1 && 
            field_index < ((gamma->height) * (gamma->width)));
} 

void set_fill_distance_of_field(uint32_t x, uint32_t y, gamma_t* gamma) {
    uint64_t nr_of_field = calculate_field_index(gamma, x, y);
    if (correct_field_index(nr_of_field, gamma)) {
        uint32_t field_owner = gamma->fields[nr_of_field]->player_owning;
        int n = numbers_length(field_owner);
        if (n > 1) {
            gamma->fields[nr_of_field]->fill_distance = n + 1;
        }
        else {
            gamma->fields[nr_of_field]->fill_distance = 0;
        }
    }
}

bool correct_prev_owner(field_t* field, uint32_t player_number) 
{
    if (field == NULL) 
        return false;
    return ((field->player_owning != 0) && 
            (field->player_owning != player_number));
}

void fill_neighbour_coordinates(long long int neighbour_x[4], 
                                long long int neighbour_y[4], 
                                long long int x, long long int y) 
{
    neighbour_x[0] = neighbour_x[2] = x;
    neighbour_x[1] = x + 1;
    neighbour_x[3] = x - 1;

    neighbour_y[1] = neighbour_y[3] = y;
    neighbour_y[0] = y + 1;
    neighbour_y[2] = y - 1; 
}

int how_many_times_appears_in_array(uint32_t x, uint32_t array[4]) 
{
    int count = 0;
    for (int i = 0; i < 4; i++) {
        if (array[i] == x)
            ++count;
    }    

    return count;
}

void fill_neighbour_arrays(uint32_t x, uint32_t y, uint32_t owners[4], 
                            long long int indexes[4], 
                            field_t* neighbour_fields[4], gamma_t* g) 
{
    long long int neighbour_x[4];
    long long int neighbour_y[4];
    fill_neighbour_coordinates(neighbour_x, neighbour_y, x, y);

    for (int i = 0; i < 4; i++) {
        indexes[i] = calculate_field_index(g, neighbour_x[i], neighbour_y[i]);    

        if (correct_field_index(indexes[i], g)) {
            owners[i] = ((g->fields)[indexes[i]])->player_owning;
            neighbour_fields[i] = (g->fields)[indexes[i]];
        }
        else {
            owners[i] = 0;
            neighbour_fields[i] = NULL;
        }
    }  
}

bool correct_parametres(uint32_t x, uint32_t y, uint32_t player, gamma_t* g) 
{
    return (x < g->width && 
            y < g->height && 
            player <= g->nr_of_players &&
            player > 0);
}

uint32_t divisor(int num_length) {
    uint32_t div = 1;
    for (int i = 0; i < num_length - 1; i++) 
        div *= 10;

    return div;
}

void insert_char(char a, char* sign, int* index) {
    sign[*index] = a;
    ++(*index);
}