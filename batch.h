/** @file
 * @brief Interfejs klasy pozwalającej na przeprowadzenie gry w trybie 
 * wsadowym.
 * @author Justyna Micota <jm418427@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 10.05.2020
 */

#ifndef BATCH_H
#define BATCH_H

/** @brief Przeprowadza grę w trybie wsadowym.
 * @param[in, out] a                – adres zmiennej przechowującej ostatni 
 *                                    wczytany znak z wejścia,
 * @param[in, out] line_counter     – adres zmiennej przechowującej numer linii,
 * @param[in, out] gamma            – adres struktury przechowującej stan gry.
 */
void start_batch(int* a, uint32_t* line_counter, gamma_t* gamma);

#endif /*BATCH_H*/