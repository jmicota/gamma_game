/** @file
 * @brief Interfejs klasy przechowującej pojedyncze operacje na planszy.
 * Operacje aktualizujące stan struktur pól oraz graczy, a także
 * dostosowana implementacja algorytmów dfs i find and union wykorzystywanych 
 * w rozwiązaniu.
 * @author Justyna Micota <jm418427@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 14.04.2020
 */

#ifndef OPERATIONS_H
#define OPERATIONS_H

/** @brief Resetuje wartość visited pól gracza @p player_number oraz pustych.
 * Zmienia wartość visited dla wszystkich pól na planszy należących
 * do gracza @p player_number oraz wszystkich pustych pól na planszy na false. 
 * @param[in] g             – wskaźnik na strukturę przechowującą stan gry,
 * @param[in] player_number     – numer gracza, liczba dodatnia niewiększa od 
 *                                wartości @p players z funkcji @ref gamma_new.
 */
void reset_visited(uint32_t player_number, gamma_t* g); 

/** @brief Tworzy od nowa unię gracza z pola (x,y).
 * Za pomocą algorytmu dfs tworzy nową unię gracza @p player_number 
 * której jako lidera ustawia pole union_leader.
 * @param[in] g             – wskaźnik na strukturę przechowującą stan gry,
 * @param[in] player_number     – numer gracza dla którego tworzona jest unia,
 * @param[in] x                 – numer kolumny dla obecnie badanego pola,
 * @param[in] y                 – numer wiersza dla obecnie badanego pola,
 * @param[in] union_leader      - pole ustanowione jako lider dla tworzonej
 *                                unii (sąsiaduje z polem na które wykonywany
 *                                jest ruch).
 */
void build_players_unions_anew(long long int x, long long int y, 
                    field_t* union_leader, uint32_t player_number, 
                    gamma_t* g);

/** @brief Zlicza różnych liderów unii sąsiadujących z polem badanym w danym
 * ruchu, należących do gracza @p player_number .
 * @param[in] player_number     – numer gracza,
 * @param[in] neighbour_fields  – czteroelementowa tablica pól sąsiadujących
 *                                z badanym polem,
 * @param[in] neighbour_owners  – czteroelementowa tablica graczy zajmujących
 *                                sąsiadujące pola,
 * pola w tablicach neighbours[4] reprezentują sąsiadujące pola w następującej
 * kolejności: górne, prawe, dolne, lewe.
 * @return Liczba różnych unii gracza player_number występujących na 
 * sąsiadujących polach.
 */
int count_neighbour_leaders_of_player(uint32_t player_number, 
                                field_t* neighbour_fields[4], 
                                uint32_t neighbour_owners[4]);

/** @brief Dodaje nienapotkane wcześniej sąsiadujące puste pola dla gracza.
 * Sprawdza, czy któreś z sąsiadujących pustych pól nie było wcześniej (przed 
 * ruchem obecnie wykonywanym) sąsiadem któregoś z pól gracza @p player_number.
 * Jesli nie było, zwiększa wartość adjacent_empty_fields dla tego gracza.
 * @param[in] moved_on_field_index  – indeks pola, na które został wykonany ruch
 * @param[in] x                     – numer kolumny pola, na które 
 *                                    wykonywany jest ruch,
 * @param[in] y                     – numer wiersza pola, na które 
 *                                    wykonywany jest ruch,
 * @param[in] player_number         – numer gracza,
 * @param[in] neighbour_indexes     – czteroelementowa tablica indeksów pól 
 *                                    sąsiadującychz badanym polem,
 * @param[in] neighbour_owners      – czteroelementowa tablica graczy zajmujących
 *                                    sąsiadujące pola,
 * @param[in] g                     – wskaźnik na strukturę przechowującą stan gry,
 * wartosci w tablicach neighbour_...[4] reprezentują sąsiadujące pola w 
 * następującej kolejności: górne, prawe, dolne, lewe.
 */
void add_new_adjacent_empty_fields_for_player_that_moved(
                                    uint32_t moved_on_field_index, uint32_t x,
                                    uint32_t y, uint32_t neighbour_owners[4],
                                    long long int neighbour_indexes[4], 
                                    uint32_t player_number, gamma_t* g);

/** @brief Zmniejsza wartość adjacent_empty_fields dla każdego gracza
 * sąsiadującego z polem, na które został wykonany ruch.
 * @param[in] g                 – wskaźnik na strukturę przechowującą stan gry,
 * @param[in] players_array     – czteroelementowa tablica graczy zajmujących
 *                                sąsiadujące pola,
 * wartosci w tablicy players_array[4] reprezentują graczy zajmujących sąsiadujące
 * pola w następującej kolejności: górne, prawe, dolne, lewe.
 */
void subtract_one_adjacent_empty_field_for_neighbours(uint32_t players_array[4], 
                                                      gamma_t* g);
                                     


/** @brief Aktualizuje unie, occupied_fields oraz nr_of_areas gracza @p player.
 * Wykonuje algorytm union dla pola na które wykonywany jest ruch oraz 
 * kolejno dla wszystkich sąsiadujących pól, których wlascicielem jest gracz
 * @p player, porównuje liczbę liderów przed i po utworzeniu unii, aktualizuje
 * liczbę powierzchni oraz pól zajmowanych przez gracza @p player.
 * @param[in] x                     – numer kolumny pola, na które 
 *                                    wykonywany jest ruch,
 * @param[in] y                     – numer wiersza pola, na które 
 *                                    wykonywany jest ruch,
 * @param[in] g                     – wskaźnik na strukturę przechowującą stan gry,
 * @param[in] player                – numer gracza,
 * @param[in] neighbour_fields      – czteroelementowa tablica sąsiadujących pól,
 * wartosci w tablicach neighbour_...[4] reprezentują sąsiadujące pola w 
 * następującej kolejności: górne, prawe, dolne, lewe.
 */
void update_player_that_moved(player_t* player, uint32_t x, uint32_t y, 
                                gamma_t* g, field_t* neighbour_fields[4]);

/** @brief Aktualizuje liczbę adjacent_empty_fields poprzedniego właściciela
 * pola, na które wykonywany jest złoty ruch.
 * Tylko dla złotego ruchu. 
 * Od liczby adjacent_empty_fields gracza prev_owner odejmowane są
 * puste pola sąsiadujące z polem, na które wykonywany jest ruch, i z żadnym 
 * innym polem należącym do prev_owner.
 * @param[in] neighbour_x           – czteroelementowa tablica numerów kolumn pól 
 *                                    sąsiadujących z polem, na które został
 *                                    wykonany ruch,
 * @param[in] neighbour_y           – czteroelementowa tablica numerów wierszy pól 
 *                                    sąsiadujących z polem, na które został
 *                                    wykonany ruch,
 * @param[in] g                     – wskaźnik na strukturę przechowującą stan gry,
 * @param[in] prev_owner            – numer poprzedniego właściciela pola,
 * @param[in] field_index           – indeks pola, na które został wykonany ruch,
 * @param[in] neighbour_owners      – czteroelementowa tablica graczy zajmujących
 *                                    sąsiadujące pola,
 * wartosci w tablicach neighbour_...[4] reprezentują sąsiadujące pola w 
 * następującej kolejności: górne, prawe, dolne, lewe.
 */
void subtract_adjacent_empty_of_prev_owner(uint32_t prev_owner, 
            uint32_t field_index, uint32_t neighbour_owners[4],
            long long int neighbour_x[4], long long int neighbour_y[4], 
            gamma_t* g);

#endif /*OPERATIONS_H*/