/** @file
 * @brief Interfejs klasy pozwalającej na przeprowadzenie gry w trybie 
 * interaktywnym.
 * @author Justyna Micota <jm418427@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 10.05.2020
 */

#ifndef INTERACTIVE_H
#define INTERACTIVE_H

/** @brief Przeprowadza grę w trybie interaktywnym.
 * @param[in, out] gamma     – adres struktury przechowującej stan gry.
 */
void start_interactive(gamma_t* gamma);

#endif /*INTERACTIVE_H*/