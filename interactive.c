#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "base.h"
#include "operations.h"
#include "gamma.h"
#include "input.h"

/* Drukuje jedno pole planszy.
 */
static void print_one_field(uint32_t field_owner, int max_oner_length) {
    int owner_length = numbers_length(field_owner);
    if (field_owner == 0) {
        printf(".");
    }
    else printf("%d", field_owner);

    for (int i = 0; i < max_oner_length - owner_length; i++) {
        printf(" ");
    }
    printf(" ");
}

/* Drukuje planszę w sposób czytelny dla użytkownika (z równego rozmiaru
 * odległościami między polami).
 */
static void interactive_print_board(gamma_t* gamma) {

    int max_length = numbers_length(gamma->nr_of_players);

    for (uint32_t i = 0; i < gamma->height; i++) {

        uint32_t line_index = i * gamma->width;

        for (uint32_t j = 0; j < gamma->width; j++) {
            print_one_field(gamma->fields[line_index + j]->player_owning, max_length);
        }
        printf("\n");
    }
}

/* Przesuwa kursor o jedno pole w lewo.
 */
static void move_left(uint32_t* column, gamma_t* gamma) {
    int max_length = numbers_length(gamma->nr_of_players);
    for (int i = 0; i < max_length + 1; i++) {
        printf("\033[D");
    }
    (*column)--;
}

/* Przesuwa kursor o jedno pole w prawo.
 */
static void move_right(uint32_t* column, gamma_t* gamma) {
    int max_length = numbers_length(gamma->nr_of_players);
    for (int i = 0; i < max_length + 1; i++) {
        printf("\033[C");
    }
    (*column)++;
}

/* Przesuwa kursor o jedno pole w górę.
 */
static void move_up(uint32_t* row) {
    printf("\033[A");
    (*row)++;
}

/* Przesuwa kursor o jedno pole w dół.
 */
static void move_down(uint32_t* row) {
    printf("\033[B");
    (*row)--;
}

/* Przesuwa kursor na środek planszy.
 */
static void set_cursor_to_middle(gamma_t* gamma, uint32_t* column, uint32_t* row) {
    *column = 0;
    *row = 0;
    for (uint32_t i = 0; i < (gamma->height)/2; i++) {
        move_up(row);
    }
    for (uint32_t i = 0; i < (gamma->width)/2; i++) {
        move_right(column, gamma);
    }
}

/* Sprawdza, czy gracz może wykonać ruch.
 */
static bool can_move(uint32_t player_number, gamma_t* gamma) {
    return (gamma_free_fields(gamma, player_number) != 0 || 
            gamma_golden_possible(gamma, player_number));
}

/* Sprawdza, czy którykolwiek z graczy może wykonać ruch.
 */
static bool gamma_finished(gamma_t* gamma) {
    if (gamma->all_free_fields == 0) {
        return true;
    }
    for (uint32_t i = 1; i <= gamma->nr_of_players; i++) {
        if (can_move(i, gamma)) {
            return false;
        }
    }
    return true;
}

/* Wyznacza numer kolejnego gracza.
 * Pomija graczy, którzy nie mogą wykonać żadnego ruchu.
 */
static uint32_t next_player_to_move(uint32_t curr_player, gamma_t* gamma) {
    if (gamma_finished(gamma)) 
        return 0;

    curr_player = curr_player + 1;
    if (curr_player > gamma->nr_of_players) {
        curr_player = 1;
    }

    // poprzednie sprawdzenie gamma_finished() zapewnia przerwanie pętli while
    while (!can_move(curr_player, gamma)) {
        curr_player = curr_player + 1;
        if (curr_player > gamma->nr_of_players) {
            curr_player = 1;
        }
    }
    return curr_player;
}

/* Wypisuje informacje o graczu w pojedynczej linii (numer gracza, 
 * liczbę zajmowanych pól, liczbę zajmowanych niesąsiadujących przestrzeni,
 * oraz znak G, jeśli gracz nie wykonał jeszcze złotego ruchu).
 */
static void print_player_state(uint32_t player_number, gamma_t* gamma) {
    
    printf("PLAYER %d %d %ld", player_number, 
            gamma->players[player_number]->occupied_areas, 
            gamma_free_fields(gamma, player_number));
    
    if (gamma->players[player_number]->used_golden_move) {
        printf("\n");
    }
    else printf(" G\n");
}

/* Wypisuje podsumowanie gry dla wszystkich graczy, każdy w
 * osobnym wierszu.
 */
static void print_summary(gamma_t* gamma) {
    for (uint32_t i = 1; i <= gamma->nr_of_players; i++) {
        printf("PLAYER %d %d\n", i, gamma->players[i]->occupied_fields);
    }
}

/* Jeśli pobrana została poprawna sekwencja z wejścia (reprezentująca
 * strzałkę), wywołuje funkcje poruszające odpowiednio kursorem.
 */
static void move_cursor(int c, uint32_t* column, uint32_t* row, 
                        gamma_t* gamma) {
    switch(c) {
        case 65: 
            if (*row < gamma->height -1) {
                move_up(row);
            }
            break;
        case 66: 
            if (*row > 0) {
                move_down(row);
            }
            break;
        case 67: 
            if (*column < gamma->width -1) {
                move_right(column, gamma);
            }
            break;
        case 68: 
            if (*column > 0) {
                move_left(column, gamma);
            }
            break;
        default:
            break;
    }
}

/* Funkcja pozwalająca na przeprowadzenie jednej z następujących
 * akcji (wprowadzenie przez gracza odpowiedniej komendy): 
 * rezygnacja z ruchu (C lub c), zakończenie gry (ctrl+D), 
 * poruszenie kursorem o jedno pole w dowolnym kierunku (strzałka),
 * wykonanie ruchu na zaznaczone pole o ile jest to możliwe (spacja), 
 * wykonanie złotego ruchu na zaznaczone pole o ile jest to możliwe (G lub g).
 * Zwraca wartość true jeśli wykonywany ruch kończy turę gracza,
 * false w przeciwnym wypadku.
 */
static bool one_move(uint32_t player_number, bool* finish_game,
              uint32_t* column, uint32_t* row, gamma_t* gamma) {
    
    *finish_game = false;
    bool move_finished = false;
    
    system("/bin/stty raw");
    system("/bin/stty -echo");

    int a = getchar();

    if (a == 'C' || a == 'c') { // rezygnacja z ruchu
        move_finished = true;
    }
    else if (a == 4) { // ctrl + D
        move_finished = *finish_game = true;
    }
    else if ((char)a == ' ') {
        move_finished = gamma_move(gamma, player_number, *column, *row);
        set_fill_distance_of_field(*column, *row, gamma);
    }
    else if ((char)a == 'G' || (char)a == 'g') {
        move_finished = gamma_golden_move(gamma, player_number, *column, *row);
        set_fill_distance_of_field(*column, *row, gamma);
    }
    else if (a == 27) { // próba pobrania strzałki z wejścia
        int b = getchar();
        if (b == 91) {
            int c = getchar();
            if (c >= 65 && c <= 68) {
                move_cursor(c, column, row, gamma);
            }
        }
    }

    system("/bin/stty echo");
    system("/bin/stty cooked");
    return move_finished;
}

/* Aktualizuje wygląd planszy oraz napis zawierający informacje
 * dotyczące gracza wykonującego ruch, ustawia kursor na środku planszy.
 */
static void reset_turn(uint32_t player_number, gamma_t* gamma, 
                  uint32_t* column, uint32_t* row) {
    system("clear");
    interactive_print_board(gamma);
    print_player_state(player_number, gamma);
    printf("\033[A"); // powrót do linii ze stanem gracza
    printf("\033[A"); // powrót na początek planszy
    set_cursor_to_middle(gamma, column, row);
}

/* Przeprowadza turę jednego gracza.
 */
static bool player_move(uint32_t player_number, gamma_t* gamma) {
    bool finish_game = false, move_done = false;
    uint32_t column = 0, row = 0;
    reset_turn(player_number, gamma, &column, &row);

    while(!move_done) {
        move_done = one_move(player_number, &finish_game, &column, &row, gamma);
    }

    if (finish_game) {
        return false;
    }
    return true;
}

void start_interactive(gamma_t* gamma) {

    uint32_t player = 1;

    // jeśli next_player_to_move() == 0, to żaden gracz nie może się poruszyć
    // player_move() == false oznacza, że zostało pobrane ctrl+D
    while (player != 0 && player_move(player, gamma)) {
        player = next_player_to_move(player, gamma);
    }

    system("clear");
    interactive_print_board(gamma);
    print_summary(gamma);
}