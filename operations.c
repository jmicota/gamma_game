#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "base.h"
#include "operations.h"

// algorithm find and union
static field_t* find_leader(gamma_t* gamma, field_t* field) 
{
    if (field == field->parent_field)
        return field;
    
    else return find_leader(gamma, field->parent_field);
}

// unions two fields if belong to the same player, 
// if different areas connected, decreases nr of players areas
static void union_fields_and_update_players_areas(field_t* field1, 
                                    field_t* field2, gamma_t* gamma) 
{
    if (field1 != NULL && field2 != NULL &&
        field1->player_owning == field2->player_owning) {

        field_t *leader1 = find_leader(gamma, field1);
        field_t *leader2 = find_leader(gamma, field2);

        if (leader1 != leader2) { // union two different (!) areas
            
            if (leader1->rank < leader2->rank) {
                leader1->parent_field = leader2;
            }
            else {
                leader2->parent_field = leader1;
                // if ranks where equal before union, increase leaders rank by 1
                if (leader1->rank == leader2->rank) { 
                    leader1->rank += 1;
                }
            }
            // different (!) areas connected, players number of areas
            // need to be decreased by 1
            uint32_t player_number = field1->player_owning;
            --((gamma->players)[player_number]->occupied_areas);
        }
    }
}

// sets value of visited for all empty fields and fields 
// belonging to player_number to false
void reset_visited(uint32_t player_number, gamma_t* g) 
{
    uint32_t all_fields = (g->width) * (g->height);

    for (uint32_t i = 0; i < all_fields; i++) {

        if ((g->fields)[i]->player_owning == 0 ||
            (g->fields)[i]->player_owning == player_number) {

            (g->fields)[i]->visited = false;
        }
    }
}

// dfs
void build_players_unions_anew(long long int x, long long int y, 
                            field_t* union_leader, uint32_t player_number, 
                            gamma_t* g) 
{
    long long int field_index = calculate_field_index(g, x, y);
    
    // if player_number owns the field and field not visited yet
    if (correct_field_index(field_index, g) &&
        (g->fields)[field_index]->player_owning == player_number &&
        !((g->fields)[field_index]->visited)) { 
        
        field_t* field = (g->fields)[field_index];
        field->parent_field = union_leader;
        field->rank = 1;
        field->visited = true;

        long long int neighbour_x[4];
        long long int neighbour_y[4];
        fill_neighbour_coordinates(neighbour_x, neighbour_y, x, y);

        for (int i = 0; i < 4; i++) {
            build_players_unions_anew(neighbour_x[i], neighbour_y[i], 
                                     union_leader, player_number, g);
        }
    }
}

int count_neighbour_leaders_of_player(uint32_t player_number, 
                                    field_t* neighbour_fields[4], 
                                    uint32_t neighbour_owners[4]) 
{
    bool leader_already_seen[4] = {false, false, false, false};
    int count = 0;

    for (int i = 0; i < 4; i++) {
        // if it belongs to player_number and has an unseen yet leader
        if (neighbour_owners[i] == player_number && !(leader_already_seen[i])) { 
            
            ++count;
            field_t* this_leader = neighbour_fields[i]->parent_field;
            // checking all the next fields with this leaders as seen
            for (int j = i; j < 4; j++) { 
                
                if (neighbour_fields[j] != NULL &&
                    neighbour_fields[j]->parent_field == this_leader) {
                    
                    leader_already_seen[j] = true;
                }
            }
        }
    }

    return count;
}

// checks if a field has other neighbour_field belonging to player_number
// (different than field_moved_on_index)
static bool was_neighbour_of_player_before_this_move(uint32_t player_number, 
                                    uint32_t field_moved_on_index, uint32_t x, 
                                    uint32_t y, gamma_t* gamma) 
{
    long long int neighbour_x[4];
    long long int neighbour_y[4];
    fill_neighbour_coordinates(neighbour_x, neighbour_y, x, y);

    uint32_t neighbour_owners[4];
    long long int neighbour_index[4];
    field_t* neighbour_fields[4];
    fill_neighbour_arrays(x, y, neighbour_owners, neighbour_index, 
                          neighbour_fields, gamma);

    for (int i = 0; i < 4; i++) {
        // if any of neighbour fields belongs to player_number
        // and is different than field_moved_on_index
        if (correct_field_index(neighbour_index[i], gamma) && 
            neighbour_owners[i] == player_number && 
            neighbour_index[i] != field_moved_on_index) {
            
            return true;
        }
    }

    return false;
}

void add_new_adjacent_empty_fields_for_player_that_moved(
                                    uint32_t moved_on_field_index, uint32_t x,
                                    uint32_t y, uint32_t neighbour_owners[4], 
                                    long long int neighbour_indexes[4], 
                                    uint32_t player_number, gamma_t* g) 
{
    long long int neighbour_x[4];
    long long int neighbour_y[4];
    fill_neighbour_coordinates(neighbour_x, neighbour_y, x, y);

    for (int i = 0; i < 4; i++) {
        if (correct_field_index(neighbour_indexes[i], g) && 
            neighbour_owners[i] == 0 && 
            !was_neighbour_of_player_before_this_move(player_number, 
                                moved_on_field_index, neighbour_x[i], 
                                neighbour_y[i], g)) {

            ++((g->players)[player_number]->adjacent_empty_fields);
        }
    }
}

void subtract_one_adjacent_empty_field_for_neighbours(uint32_t players_array[4],
                                                      gamma_t* g) 
{
    uint32_t players_already_updated[4] = {0, 0, 0, 0};

    for (int i = 0; i < 4; i++) {
        // if neighbour owner (!= 0) hasnt been updated yet
        if (how_many_times_appears_in_array(players_array[i], 
                                players_already_updated) == 0 &&
            players_array[i] != 0) 
        {
            // update his adjacent empty fields
            (g->players)[players_array[i]]->adjacent_empty_fields -= 1; 
            // add to array of updated
            players_already_updated[i] = players_array[i]; 
        }
    }
}

void update_player_that_moved(player_t* player, uint32_t x, uint32_t y, 
                                gamma_t* g, field_t* neighbour_fields[4]) 
{
    long long int field_index = calculate_field_index(g, x, y);
    ++(player->occupied_fields);
    ++(player->occupied_areas); 
    // ^always create new area (will decrease whith unions)
    
    for (int i = 0; i < 4; i++) {
        union_fields_and_update_players_areas((g->fields)[field_index], 
                                                neighbour_fields[i], g);
    }
}

void subtract_adjacent_empty_of_prev_owner(uint32_t prev_owner, 
                uint32_t field_index, uint32_t neighbour_owners[4],
                long long int neighbour_x[4], long long int neighbour_y[4], 
                gamma_t* g) 
{
    player_t* prev_owner_struct = (g->players)[prev_owner];
    for (int i = 0; i < 4; i++) {
        if (neighbour_owners[i] == 0 &&
            !was_neighbour_of_player_before_this_move(prev_owner, field_index, 
                                            neighbour_x[i], neighbour_y[i], g)) {
            // if some neighbour empty field of prev_owner was touched only
            // by the field thats being taken away from him

            --(prev_owner_struct->adjacent_empty_fields);
        }
    }
}