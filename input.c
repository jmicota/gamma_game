#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "base.h"
#include "operations.h"
#include "gamma.h"

// checks if a character is a white character
static bool is_white(int a) {
    if (a == EOF) return false;
    char c = (char)a;
    if (c == ' ' || c == '\t' ||
        c == '\r' || c == '\v' ||
        c == '\f') {
        
        return true;
    }
    return false;
}

static bool is_digit(int a) {
    if (47 < a && a < 58) {
        return true;
    }
    return false;
}

static int number_from_ascii(int code) {
    return code - 48;
}

void reset_values(char* mode, uint32_t* arg1, uint32_t* arg2, uint32_t* arg3) {
    *mode = 'x';
    *arg1 = 0;
    *arg2 = 0;
    *arg3 = 0;
}

// skips all white characters
// returns ascii code of next non_white character from input or EOF
static void get_non_white(int* a) {
    while (*a != EOF && is_white(*a)) {
        *a = getchar();
    }
}

void skip_line(int* a) {
    if (*a != EOF && *a != '\n') {
        *a = getchar();
        while (*a != EOF && (char)(*a) != '\n') {
            *a = getchar();
        }
    }
}

// assigns next integer value from input to 'value'
// returns false if next string in input isnt a number
// or if there was no number in line to assign
static uint32_t get_int(int* a, bool* error) {
    uint32_t ret = 0;
    if (!is_white(*a)) { // forcing space between arguments
        *error = true;
    }
    else {
        get_non_white(a);
        while(*a != EOF && (char)(*a) != '\n' && is_digit(*a) && !(*error)) {
            int n = number_from_ascii(*a);
            if (ret == 0) {
                ret = n;
            }
            else if ((UINT32_MAX - n) / 10 < ret) {
                *error = true;
            }
            else {
                ret *= 10;
                ret += n;
            }
            *a = getchar();
        }
        if (*a != EOF && (char)(*a) != '\n' && 
            !is_white(*a) && !is_digit(*a)) {

            ret = 0;
        }
    }
    return ret;
}

void check_last_character(int* a, bool* error) {
    if (!(*error)) {
        if (*a != EOF && (char)(*a) != '\n') {
            get_non_white(a);
        }
        *error = ((char)(*a) != '\n');
    }
}

static bool error_mode_first_line(int a) {
    bool ret_error = false;
    if (a == EOF) {
        ret_error = true;
    }
    else {
        char c = (char)a;
        if (c == 'B' || c == 'I' || c == '#' || c == '\n') {
            ret_error = false;
        }
        else ret_error = true;
    }
    return ret_error;
}

static bool error_mode_command_line(int a) {
    bool ret_error = false;
    if (a == EOF) {
        ret_error = true;
    }
    else {
        char c = (char)a;
        if (c == '#' || c == 'm' || c == 'g' || c == 'b' ||
            c == 'f' || c == 'q' || c == 'p' || c == '\n') {
            ret_error = false;
        }
        else ret_error = true;
    }
    return ret_error;
}

void assign_mode(int* a, char* mode, bool* error, bool if_first_line) {
    if ((*a) != EOF) {
        *mode = (char)(*a);
    }
    if (if_first_line) {
        *error = error_mode_first_line(*a);
    }
    else {
        *error = error_mode_command_line(*a);
    }
}

void assign_number(int* a, uint32_t* where, bool* error) {
    if (!(*error) && *a != EOF && *a != '\n') {
        *where = get_int(a, error);
        if (!error) {
            *error = (*where == 0);
        }
    }
    else {
        *error = true;
    }
}

bool get_first_line(uint32_t* line_counter, int* a, char* mode, uint32_t* width,
                    uint32_t* height, uint32_t* players, uint32_t* areas) {
    (*line_counter)++;
    bool error = false;
    
    *a = getchar();

    // if line starts with EOF only, its a correct (empty) line, 
    // but getting input has to be stopped
    if (*a == EOF) { 
        return true;
    }

    assign_mode(a, mode, &error, 1);
    if (*mode == '#' || error || *mode == '\n') {
        skip_line(a);
    }
    else if (*a != EOF) {
        *a = getchar();
        if (!is_white(*a)) {
            error = true;
        }
        assign_number(a, width, &error);
        assign_number(a, height, &error);
        assign_number(a, players, &error);
        assign_number(a, areas, &error);
        check_last_character(a, &error);
        skip_line(a);
        // if line doesnt end with '\n'
        if (*a == EOF) {
            error = true;
        }
    }
    
    
    return !error;
}