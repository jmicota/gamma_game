/** @file
 * @brief Interfejs klasy przechowującej operacje zarządzające wejściem programu.
 * @author Justyna Micota <jm418427@students.mimuw.edu.pl>
 * @copyright Uniwersytet Warszawski
 * @date 10.05.2020
 */

#ifndef INPUT_H
#define INPUT_H

/** @brief Przypisuje wartość pierwszego znaku w linii na adres zmiennej mode.
 * Sprawdza czy znak jest właściwy, zależnie od tego, czy linia ma determinować
 * tryb i parametry gry, czy konkretne polecenie, zapisuje informacje o błędach
 * na zmienną 'error'.
 * @param[in, out] a                – adres zmiennej przechowującej ostatni 
 *                                    wczytany znak z wejścia,
 * @param[in] mode                  – adres zmiennej przechowującej pierwszy
 *                                    znak linii odpowiadającej za wybór
 *                                    trybu gry,
 * @param[in] if_first_line         – wartość @p true nakazuje oczekiwać znaków
 *                                    poprawnych dla komendy rozpoczynającej
 *                                    grę, wartość @p false nakazuje oczekiwać
 *                                    znaku odpowiedniej komendy trybu wsadowego,
 * @param[in, out] error            – adres zmiennej przechowującej
 *                                    informacje o błędach.
 */
void assign_mode(int* a, char* mode, bool* error, bool if_first_line);

/** @brief Resetuje wartości argumentów.
 * Wszystkie wartość typu uint32_t ustawia na 0, 
 * wartość mode na 'x' (niepoprawną w każdej sytuacji).
 */
void reset_values(char* mode, uint32_t* arg1, uint32_t* arg2, uint32_t* arg3);

/** @brief Pomija jedną linię wejścia (aż do wystąpienia znaku '\n' lub EOF).
 * @param[in, out] a                – adres zmiennej przechowującej ostatni 
 *                                    wczytany znak z wejścia.
 */
void skip_line(int* a);

/** @brief Zapisuje wartość typu uint32_t na adres zmiennej 'where'. 
 * Wywołuje funkcję pobierającą liczbę z wejścia i zwracającą informacje
 * o ewentualnych błędach na adres zmiennej 'error'.
 * Nie wykonuje żadnej akcji, jeśli wartość pod adresem 'error' jest 
 * ustawiona na @p true.
 * @param[in, out] a                – adres zmiennej przechowującej ostatni 
 *                                    wczytany znak z wejścia,
 * @param[in, out] where            – adres zmiennej na którą zapisana zostanie
 *                                    pobrana z wejścia wartość (o ile poprawna),
 * @param[in, out] error            – adres zmiennej przechowującej
 *                                    informacje o błędach.
 */
void assign_number(int* a, uint32_t* where, bool* error);

/** @brief Sprawdza, czy ostatni znak w linii jest właściwy (\n lub EOF), oraz
 * czy po pobraniu żądanej liczby argumentów znajduje się w linii jakiś
 * zbędny niebiały znak.
 * W wypadku wystąpienia błędu zmienia 
 * @param[in, out] a                – adres zmiennej przechowującej ostatni 
 *                                    wczytany znak z wejścia,
 * @param[in, out] error            – adres zmiennej przechowującej
 *                                    informacje o błędach.
 */
void check_last_character(int* a, bool* error);

/** @brief Zapisuje argumenty podane w pierwszej linii programu do 
 * odpowiednich zmiennych oraz ostatni znak linii (\n lub EOF) do zmiennej a.
 * @param[in, out] line_counter     – adres zmiennej przechowującej numer linii,
 * @param[in, out] a                – adres zmiennej przechowującej ostatni 
 *                                    wczytany znak z wejścia,
 * @param[out] mode                 – adres zmiennej przechowującej pierwszy
 *                                    znak linii (B lub I) odpowiadającej za
 *                                    wybór trybu gry,
 * @param[out] width                – adres zmiennej przechowującej podaną
 *                                    na wejściu żądaną szerokość planszy,
 * @param[out] height               – adres zmiennej przechowującej podaną
 *                                    na wejściu żądaną wysokość planszy,
 * @param[out] players              – adres zmiennej przechowującej liczbę
 *                                    graczy podaną na wejściu,
 * @param[out] areas                – adres zmiennej przechowującej maksymalną
 *                                    liczbę obszarów, jakie może zająć jeden 
 *                                    gracz.
 * @return Wartość @p true, gdy linia jest poprawna, wartość @p false w 
 * przeciwnym przypadku.
 */
bool get_first_line(uint32_t* line_counter, int* a, char* mode, uint32_t* width, uint32_t* height, 
                    uint32_t* players, uint32_t* areas);



#endif /*INPUT_H*/