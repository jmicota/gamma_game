#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "base.h"
#include "operations.h"
#include "gamma.h"


gamma_t* gamma_new(uint32_t width, uint32_t height, 
                    uint32_t players, uint32_t areas) 
{
    if (width > 0 && height > 0 && 
        UINT32_MAX / height > 1 &&
        UINT32_MAX / width > 1 &&
        players > 0 && areas > 0) {

        gamma_t* gamma = malloc(sizeof(gamma_t));
        if (gamma == NULL)
            return NULL;
        
        gamma->nr_of_players = players;
        gamma->height = height;
        gamma->width = width;
        gamma->areas = areas;
        gamma->all_free_fields = height * width;

        gamma->fields = malloc((gamma->all_free_fields) * sizeof(field_t*));
        if (gamma->fields == NULL) {
            gamma_delete(gamma);
            return NULL;
        }
        for (uint32_t i = 0; i < (gamma->all_free_fields); i++) {
            (gamma->fields)[i] = field_new();
        }

        gamma->players = malloc((players + 1) * sizeof(player_t*));
        if (gamma->players == NULL) {
            gamma_delete(gamma);
            return NULL;
        }
        for (uint32_t i = 0; i < players + 1; i++) {
            (gamma->players)[i] = player_new();
        }

        return gamma;
    }

    return NULL;
}


// deletes the game g
void gamma_delete(gamma_t *g) 
{
    if (g != NULL) {
        uint32_t size_2d = (g->height)  * (g->width);

        if (g->fields != NULL) {
            for(uint32_t i = 0; i < size_2d; i++) {
                free((g->fields)[i]);
            }
            free(g->fields);
        }
        
        if ((g->players) != NULL) {
            for(uint32_t i = 0; i < (g->nr_of_players + 1); i++) {
                free((g->players)[i]);
            }
            free(g->players);
        }
        
        free(g);
    }
}

bool gamma_move(gamma_t *g, uint32_t player_number, uint32_t x, uint32_t y) 
{
    if (!correct_parametres(x, y, player_number, g))
        return false;

    uint32_t field_index = calculate_field_index(g, x, y);
    field_t* field = (g->fields)[field_index];
    player_t* player = (g->players)[player_number];
    uint32_t neighbour_owners[4];
    long long int neighbour_indexes[4];
    field_t* neighbour_fields[4];
    fill_neighbour_arrays(x, y, neighbour_owners, neighbour_indexes, 
                        neighbour_fields, g);

    if (field->player_owning != 0 ||
        // check if field is 0 and if new area can be taken
        (how_many_times_appears_in_array(player_number, neighbour_owners) == 0 &&
                                        player->occupied_areas >= g->areas)) { 
        
        return false;
    }

    field->player_owning = player_number; // place a pawn
    --(g->all_free_fields);

    subtract_one_adjacent_empty_field_for_neighbours(neighbour_owners, g);
    update_player_that_moved(player, x, y, g, neighbour_fields);
    add_new_adjacent_empty_fields_for_player_that_moved(field_index, x, y,
                    neighbour_owners, neighbour_indexes, player_number, g);

    return true;
}

uint64_t gamma_busy_fields(gamma_t *g, uint32_t player) 
{
    return (g->players)[player]->occupied_fields;
}

uint64_t gamma_free_fields(gamma_t *g, uint32_t player) 
{
    player_t* player_struct = (g->players)[player];

    if ( player_struct->occupied_areas < g->areas ) {
        return g->all_free_fields;
    }
    return player_struct->adjacent_empty_fields;
}

bool gamma_golden_possible(gamma_t *g, uint32_t player) 
{
    player_t* player_struct = (g->players)[player];
    uint32_t all_fields = (g->height) * (g->width);

    if (!(player_struct->used_golden_move) && 
        (g->all_free_fields != all_fields - player_struct->occupied_fields)) {
        return true;
    }
    return false;
}

// always returning fully updated prev_owner
// checks if golden_move can be performed (if it doesnt separate prev_owners 
// areas into more than is allowed)
// HOW IT WORKS: 
// - sets fields owner to new_player 
// - by building unions anew checks, if the move is legal
// - if not, reverses change of fields and unions it back with prev_owner areas
// - if it is, updates prev_owners occupied_fields and adjacent_empty_fields
static bool previous_owner_areas_permit_golden_move(uint32_t new_player, 
                        uint32_t prev_owner, uint32_t x, uint32_t y, 
                        uint32_t neighbour_owners[4], field_t* neighbour_fields[4], 
                        gamma_t* g)
{
    long long int neighbour_x[4];
    long long int neighbour_y[4];
    fill_neighbour_coordinates(neighbour_x, neighbour_y, x, y);
    uint32_t field_index = calculate_field_index(g, x, y);
    field_t* field_struct = (g->fields)[field_index];
    player_t* prev_owner_struct = (g->players)[prev_owner];

    // placing a pawn
    field_struct->player_owning = new_player;
    field_struct->parent_field = field_struct;
    field_struct->rank = 0;
    --(prev_owner_struct->occupied_fields);

    // checks if meets at least two fields of prev_owner
    if (how_many_times_appears_in_array(prev_owner, neighbour_owners) >= 2) { 
        
        for (int i = 0; i < 4; i++) {
            if (neighbour_owners[i] == prev_owner) {
                // doesnt update players areas, only changes field leaders
                build_players_unions_anew(neighbour_x[i], neighbour_y[i], 
                                        neighbour_fields[i], prev_owner, g);
            }
        }
        reset_visited(prev_owner, g);

        int pre_neighbour_leaders = count_neighbour_leaders_of_player(prev_owner,
                                             neighbour_fields, neighbour_owners);
        prev_owner_struct->occupied_areas += (pre_neighbour_leaders - 1);

        // check if building areas anew didnt make player have too many areas
        if (prev_owner_struct->occupied_areas <= g->areas) {
            subtract_adjacent_empty_of_prev_owner(prev_owner, field_index, 
                            neighbour_owners, neighbour_x, neighbour_y, g);
            return true;
        }

        // else: move is illegal, reversing it (no need to update adjacent empty)
        field_struct->player_owning = prev_owner;
        update_player_that_moved(prev_owner_struct, x, y, g, neighbour_fields);
        
        return false;
    }

    if (how_many_times_appears_in_array(prev_owner, neighbour_owners) == 0) { 
        // then its a lonely (unconnected) prev_owners field
        --(prev_owner_struct->occupied_areas);
    }

    subtract_adjacent_empty_of_prev_owner(prev_owner, field_index, 
                    neighbour_owners, neighbour_x, neighbour_y, g);
    return true;
}

bool gamma_golden_move(gamma_t *g, uint32_t player, uint32_t x, uint32_t y) 
{
    if (!correct_parametres(x, y, player, g)) {
        return false;
    }

    uint32_t neighbour_owners[4];
    long long int neighbour_indexes[4];
    field_t* neighbour_fields[4];
    fill_neighbour_arrays(x, y, neighbour_owners, neighbour_indexes, 
                        neighbour_fields, g);

    uint32_t field_index = calculate_field_index(g, x, y);
    uint32_t prev_owner = (g->fields)[field_index]->player_owning;
    player_t* new_owner_struct = (g->players)[player];
    field_t* field_struct = (g->fields)[field_index];
    
    if (!gamma_golden_possible(g, player) ||
        !correct_prev_owner(field_struct, player)) {
        return false;
    }
    
    if (how_many_times_appears_in_array(player, neighbour_owners) == 0
        && new_owner_struct->occupied_areas >= g->areas) {
        return false;
    }

    long long int neighbour_x[4];
    long long int neighbour_y[4];
    fill_neighbour_coordinates(neighbour_x, neighbour_y, x, y);

    // placing new players pawn
    field_struct->player_owning = player;
    field_struct->parent_field = field_struct;
    field_struct->rank = 0;

    if (!previous_owner_areas_permit_golden_move(player, prev_owner, x, y, 
                                    neighbour_owners, neighbour_fields, g)) {
        // this function updates prev_player for this move
        return false;
    }
    
    new_owner_struct->used_golden_move = true;

    update_player_that_moved(new_owner_struct, x, y, g, neighbour_fields);
    add_new_adjacent_empty_fields_for_player_that_moved(field_index, x, y,
                            neighbour_owners, neighbour_indexes, player, g);

    return true;
}

char* gamma_board(gamma_t *g) 
{
    int last_player_length = numbers_length(g->nr_of_players);
    uint32_t size_2d = (g->height)  * (g->width);
    uint32_t max_size = size_2d * (last_player_length + 2) + (g->height) + 1;
    char *sign =  malloc(max_size * sizeof(char));

    if (sign == NULL)
        return NULL;

    int j = 0;
    for (uint32_t i = 0; i < size_2d; i++) { // for all fields

        uint32_t player_number = (g->fields)[i]->player_owning;
        int num_length = numbers_length(player_number);
        uint32_t div = divisor(num_length);

        if (player_number == 0) {
            insert_char('.', sign, &j);
        }
        else {
            if (num_length > 1)
                insert_char('[', sign, &j);

            int tmp = num_length;
            while (tmp > 0) {
                int n = player_number / div;
                player_number = player_number % div;
                div = div / 10;
                --tmp;

                char a = (char)(n + 48);
                insert_char(a, sign, &j);
            }

            if (num_length > 1)
                insert_char(']', sign, &j);
        }

        // if end of one line
        if ((i > 0 && ((i + 1) % (g->width)) == 0) || g->width == 1)
            insert_char('\n', sign, &j);
    }
    insert_char('\0', sign, &j);
    
    return sign;
}